module intersect_ad #(parameter N = 16, parameter AUTO_DISABLE = 0, parameter BALL_WIDTH = 5, parameter BALL_HEIGHT = 5) (input en, input clk, input rst, input [N-1:0] box_x, input [N-1:0]  box_y, input [N-1:0]  box_w, 
                                       input [N-1:0]  box_h, input [N-1:0]  ball_x,  input [N-1:0]   ball_y,
                                       output logic intersection, output logic bw_x, output logic vertical_hit);

    always @(posedge clk) begin

            if (rst) begin
                            
                bw_x <= 1;
                intersection <= 0;
                vertical_hit <= 0;
            
            end
            
            else if (en) begin
    
                if (( (ball_x + BALL_WIDTH) >= box_x && ball_x <= (box_x +  box_w) ) && (ball_y + BALL_HEIGHT >= box_y && (ball_y <= box_y + box_h))) begin                
                    
                    if (ball_x < box_x && ball_x < (box_x + box_w) || (ball_x + BALL_WIDTH) > box_x && (ball_x + BALL_WIDTH) > (box_x + box_w)  ) begin

                        vertical_hit <= 1;

                    end                    

                    intersection <= 1;  
                    bw_x <= 1;

                end                          
                
                else begin 
                    bw_x <= 1;
                    intersection <= 0;  
                    vertical_hit <= 0;
                end

            end

            else begin 
                intersection <= 0;
                bw_x <= 0;
            end

    end

endmodule


