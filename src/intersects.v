module intersects #(parameter N = 16, parameter AUTO_DISABLE = 0) (input [N-1:0] box_x, input [N-1:0]  box_y, input [N-1:0]  box_w, 
                                       input [N-1:0]  box_h, input [N-1:0]  ball_x,  input [N-1:0]   ball_y,
                                       output logic intersection, input en);

    always @(*) begin
           
            if (en && ball_x >= box_x && ball_x <= (box_x +  box_w) && ball_y >= box_y && (ball_y <= box_y + box_h)) begin
                
                intersection <= 1;    

            end
            else intersection <= 0;
    end

endmodule


