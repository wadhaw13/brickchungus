module TOP
(
	input			nRST,
    input           XTAL_IN,
    input           BTN_A,
    input           BTN_B,

	output			LCD_CLK,
	output			LCD_HYNC,
	output			LCD_SYNC,
	output			LCD_DEN,
	output	[4:0]	LCD_R,
	output	[5:0]	LCD_G,
	output	[4:0]	LCD_B,
	output	logic	LED_R,
	output	logic	LED_G,
	output	logic	LED_B,

    input           KEY

);

	wire		CLK_SYS;	
	wire		CLK_PIX;

    wire        oscout_o;

    Gowin_PLL chip_pll(
        .clkout(CLK_SYS), //output clkout      //200M
        .clkoutd(CLK_PIX), //output clkoutd   //33.33M
        .clkin(XTAL_IN) //input clkin
    );	


	VGAMod	D1
	(
		.CLK		(	CLK_SYS     ),
		.nRST		(	nRST		),
    	.BTN_A		(	!BTN_A 		),
        .BTN_B		(	!BTN_B 		),

		.PixelClk	(	CLK_PIX		),
		.LCD_DE		(	LCD_DEN	 	),
		.LCD_HSYNC	(	LCD_HYNC 	),
    	.LCD_VSYNC	(	LCD_SYNC 	),

		.LCD_B		(	LCD_B		),
		.LCD_G		(	LCD_G		),
		.LCD_R		(	LCD_R		),
        .INTERSECTED(   BALL        )

	);

	assign		LCD_CLK		=	CLK_PIX;
    assign LED_R = 1;
    assign LED_G = BTN_A;
    assign LED_B = BTN_B;

endmodule