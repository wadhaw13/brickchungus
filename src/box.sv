module box #(parameter Y_LOC = 16'd0, parameter X_LOC = 16'd182, parameter WIDTH = 15, parameter HEIGHT = 15) ( input rst, input clk, input c_en, input wr_en, 
                input [15:0] i_x, input [15:0] i_y, input [15:0] colin, 
                output logic [15:0] o_x, output logic [15:0] o_y, output  logic [15:0] colour, 
                output logic [15:0] width, output logic [15:0] height);

    always@(posedge clk) begin

        width <= WIDTH;
        height <= HEIGHT;        

        if (rst) begin
            o_x <= X_LOC;
            o_y <= Y_LOC;       
            colour <= {5'b11111, 6'b0, 5'b0};
        end    

        else if (wr_en) begin            
            o_x <= i_x;
            o_y <= i_y;            
        end

        else if (c_en) colour <= colin;

    end

endmodule