module VGAMod
(
    input                   CLK,
    input                   BTN_A,
    input                   BTN_B,
    input                   nRST,
    input                   PixelClk,
    output                  LCD_DE,
    output                  LCD_HSYNC,
    output                  LCD_VSYNC,
	output     logic     [4:0]   LCD_B,
	output     logic     [5:0]   LCD_G,
	output     logic     [4:0]   LCD_R, 
    output     logic        INTERSECTED
    
);

    parameter WIDTH = 32'd662;
    parameter HEIGHT = 32'd182;
    parameter ZERO = 32'd182;
    parameter BLOCKS_ON_TOP = 32'd7;
    parameter NUM_BOXES = BLOCKS_ON_TOP*(BLOCKS_ON_TOP+1)/2;
    parameter H_DIST = 32'd65;    
    parameter BOX_WIDTH = 50;
    parameter V_DIST = 25;
    parameter V_OFFSET = 15;

    reg         [15:0]  PixelCount;
    reg         [15:0]  LineCount;

	localparam      V_BackPorch = 16'd0; 
	localparam      V_Pluse 	= 16'd5; 
	localparam      HightPixel  = 16'd480;
	localparam      V_FrontPorch= 16'd45; 

	localparam      H_BackPorch = 16'd182; 	
	localparam      H_Pluse 	= 16'd1; 
	localparam      WidthPixel  = 16'd800;
	localparam      H_FrontPorch= 16'd210;

    localparam      PixelForHS  =   WidthPixel + H_BackPorch + H_FrontPorch;  	
    localparam      LineForVS   =   HightPixel + V_BackPorch + V_FrontPorch;

    always @(  posedge PixelClk or negedge nRST  )begin
        if( !nRST ) begin
            LineCount       <=  16'b0;    
            PixelCount      <=  16'b0;
            end
        else if(  PixelCount  ==  PixelForHS ) begin
            PixelCount      <=  16'b0;
            LineCount       <=  LineCount + 1'b1;
            end
        else if(  LineCount  == LineForVS  ) begin
            LineCount       <=  16'b0;
            PixelCount      <=  16'b0;
            end
        else
            PixelCount      <=  PixelCount + 1'b1;
    end



    assign  LCD_HSYNC = (( PixelCount >= H_Pluse)&&( PixelCount <= (PixelForHS-H_FrontPorch))) ? 1'b0 : 1'b1;
    
	assign  LCD_VSYNC = ((( LineCount  >= V_Pluse )&&( LineCount  <= (LineForVS-0) )) ) ? 1'b0 : 1'b1;
    
    assign  LCD_DE = (  ( PixelCount >= H_BackPorch )&&
                        ( PixelCount <= PixelForHS-H_FrontPorch ) &&
                        ( LineCount  >= V_BackPorch ) &&
                        ( LineCount  <= LineForVS-V_FrontPorch-1 ))  ? 1'b1 : 1'b0;						

    logic [15:0] i_x;
    logic [15:0] i_y;
    logic [15:0] red_x;
    logic [15:0] red_y;
    logic [15:0] red_w;
    logic [15:0] red_h;
    logic [15:0] colour_box;
    logic [15:0] colin;
    logic wr_en;
    logic c_en;

    generate 
        box red_box(!nRST, PixelClk, c_en, wr_en, i_x, i_y, colin, red_x, red_y, colour_box, red_w, red_h);   
        defparam red_box.X_LOC = 1.5*ZERO;
        defparam red_box.Y_LOC = 3*V_DIST;
        defparam red_box.WIDTH = 5;
        defparam red_box.HEIGHT = 5;
    endgenerate

    logic [15:0] i_bx [NUM_BOXES-1:0];
    logic [15:0] i_by [NUM_BOXES-1:0];
    logic [15:0] b_x [NUM_BOXES-1:0];
    logic [15:0] b_y [NUM_BOXES-1:0];
    logic [15:0] b_w [NUM_BOXES-1:0];
    logic [15:0] b_h [NUM_BOXES-1:0];
    logic [15:0] colour_boxes [NUM_BOXES-1:0];
    logic [15:0] b_colin [NUM_BOXES-1:0];
    logic b_wr_en [NUM_BOXES-1:0];
    logic b_c_en [NUM_BOXES-1:0];
    logic [NUM_BOXES-1:0] intersection;
    logic [NUM_BOXES-1:0] intersection_with_ball;
    wire [NUM_BOXES-1:0] bw_x;    
    logic [NUM_BOXES-1:0] en; 
    wire [NUM_BOXES-1:0] verti; 
    
    logic [15:0] i_px;
    logic [15:0] i_py;
    logic [15:0] p_x;
    logic [15:0] p_y;
    logic [15:0] p_w;
    logic [15:0] p_h;
    logic [15:0] p_colour;
    logic [15:0] p_colin;
    logic p_wr_en;
    logic p_c_en;    
    logic p_intersection_with_ball;               
    logic p_intersection;               

    logic DEAD;
    logic [7:0] SPEED;

    generate  
    
        genvar i;
        genvar j; 
        parameter OFFSET = (WIDTH -(H_DIST*7) -  ZERO)/2;               
        for (i = 0 ; i < 7; i++) begin
        
             box my_box(!nRST, PixelClk, b_c_en[i], b_wr_en[i], i_bx[i], i_by[i], b_colin[i], b_x[i], b_y[i], colour_boxes[i], b_w[i], b_h[i]);
             defparam my_box.X_LOC = H_DIST*i + OFFSET + ZERO;             
             defparam my_box.WIDTH = BOX_WIDTH;
             defparam my_box.Y_LOC = V_DIST*0 + V_OFFSET;
             intersects inter(b_x[i], b_y[i], b_w[i], b_h[i], PixelCount, LineCount, intersection[i], bw_x[i]);
             intersect_ad inter2(en[i],PixelClk, !nRST, b_x[i], b_y[i], b_w[i], b_h[i], red_x, red_y, intersection_with_ball[i], bw_x[i], verti[i]);                          

        end                    

        parameter OFFSET2 = (WIDTH -(H_DIST*6) - ZERO)/2;
        for (i = 7 ; i < 13; i++) begin
        
             box my_box(!nRST, PixelClk, b_c_en[i], b_wr_en[i], i_bx[i], i_by[i], b_colin[i], b_x[i], b_y[i], colour_boxes[i], b_w[i], b_h[i]);
             defparam my_box.X_LOC = H_DIST*(i-7) + OFFSET2 + ZERO;             
             defparam my_box.WIDTH = BOX_WIDTH;
             defparam my_box.Y_LOC = V_DIST*1 + V_OFFSET;
             intersects inter(b_x[i], b_y[i], b_w[i], b_h[i], PixelCount, LineCount, intersection[i], bw_x[i]);
             intersect_ad inter2(en[i],PixelClk, !nRST, b_x[i], b_y[i], b_w[i], b_h[i], red_x, red_y, intersection_with_ball[i], bw_x[i], verti[i]);             

        end    
             
        parameter OFFSET3 = (WIDTH -(H_DIST*5) - ZERO)/2;
        for (i = 13 ; i < 18; i++) begin
        
             box my_box(!nRST, PixelClk, b_c_en[i], b_wr_en[i], i_bx[i], i_by[i], b_colin[i], b_x[i], b_y[i], colour_boxes[i], b_w[i], b_h[i]);
             defparam my_box.X_LOC = H_DIST*(i-13) + OFFSET3 + ZERO;             
             defparam my_box.WIDTH = BOX_WIDTH;
             defparam my_box.Y_LOC = V_DIST*2 + V_OFFSET;
             intersects inter(b_x[i], b_y[i], b_w[i], b_h[i], PixelCount, LineCount, intersection[i], bw_x[i]);
             intersect_ad inter2(en[i],PixelClk, !nRST, b_x[i], b_y[i], b_w[i], b_h[i], red_x, red_y, intersection_with_ball[i], bw_x[i], verti[i]);             


        end 

        parameter OFFSET4 = (WIDTH -(H_DIST*4) - ZERO)/2;
        for (i = 18 ; i < 22; i++) begin
        
             box my_box(!nRST, PixelClk, b_c_en[i], b_wr_en[i], i_bx[i], i_by[i], b_colin[i], b_x[i], b_y[i], colour_boxes[i], b_w[i], b_h[i]);
             defparam my_box.X_LOC = H_DIST*(i-18) + OFFSET4 + ZERO;             
             defparam my_box.WIDTH = BOX_WIDTH;
             defparam my_box.Y_LOC = V_DIST*3 + V_OFFSET;
             intersects inter(b_x[i], b_y[i], b_w[i], b_h[i], PixelCount, LineCount, intersection[i], bw_x[i]);
             intersect_ad inter2(en[i],PixelClk, !nRST, b_x[i], b_y[i], b_w[i], b_h[i], red_x, red_y, intersection_with_ball[i], bw_x[i], verti[i]);             

        end 

        parameter OFFSET5 = (WIDTH -(H_DIST*3) - ZERO)/2;
        for (i = 22 ; i < 25; i++) begin
        
             box my_box(!nRST, PixelClk, b_c_en[i], b_wr_en[i], i_bx[i], i_by[i], b_colin[i], b_x[i], b_y[i], colour_boxes[i], b_w[i], b_h[i]);
             defparam my_box.X_LOC = H_DIST*(i-22) + OFFSET5 + ZERO;             
             defparam my_box.WIDTH = BOX_WIDTH;
             defparam my_box.Y_LOC = V_DIST*4 + V_OFFSET;
             intersects inter(b_x[i], b_y[i], b_w[i], b_h[i], PixelCount, LineCount, intersection[i], bw_x[i]);
             intersect_ad inter2(en[i],PixelClk, !nRST, b_x[i], b_y[i], b_w[i], b_h[i], red_x, red_y, intersection_with_ball[i], bw_x[i], verti[i]);             

        end 

        parameter OFFSET6 = (WIDTH -(H_DIST*2) - ZERO)/2;
        for (i = 25 ; i < 27; i++) begin
        
             box my_box(!nRST, PixelClk, b_c_en[i], b_wr_en[i], i_bx[i], i_by[i], b_colin[i], b_x[i], b_y[i], colour_boxes[i], b_w[i], b_h[i]);
             defparam my_box.X_LOC = H_DIST*(i-25) + OFFSET6 + ZERO;             
             defparam my_box.WIDTH = BOX_WIDTH;
             defparam my_box.Y_LOC = V_DIST*5 + V_OFFSET;
             intersects inter(b_x[i], b_y[i], b_w[i], b_h[i], PixelCount, LineCount, intersection[i], bw_x[i]);
             intersect_ad inter2(en[i],PixelClk, !nRST, b_x[i], b_y[i], b_w[i], b_h[i], red_x, red_y, intersection_with_ball[i], bw_x[i], verti[i]);             

        end 

        parameter OFFSET7 = (WIDTH -(H_DIST*1) - ZERO)/2;
        for (i = 27 ; i < 28; i++) begin
        
             box my_box(!nRST, PixelClk, b_c_en[i], b_wr_en[i], i_bx[i], i_by[i], b_colin[i], b_x[i], b_y[i], colour_boxes[i], b_w[i], b_h[i]);
             defparam my_box.X_LOC = H_DIST*(i-27) + OFFSET7 + ZERO;             
             defparam my_box.WIDTH = BOX_WIDTH;
             defparam my_box.Y_LOC = V_DIST*6 + V_OFFSET;
             intersects inter(b_x[i], b_y[i], b_w[i], b_h[i], PixelCount, LineCount, intersection[i], bw_x[i]);
             intersect_ad inter2(en[i],PixelClk, !nRST, b_x[i], b_y[i], b_w[i], b_h[i], red_x, red_y, intersection_with_ball[i], bw_x[i], verti[i]);             

        end        

        box p_box(!nRST, PixelClk, p_c_en, p_wr_en, i_px, i_py, p_colin, p_x, p_y, p_colour, p_w, p_h);        
        defparam p_box.WIDTH = 2*BOX_WIDTH;        
        intersects p_inter(p_x, p_y, p_w, p_h, PixelCount, LineCount, p_intersection, 1);
        intersect_ad p_inter2(1,PixelClk, !nRST, p_x, p_y, p_w, p_h, red_x, red_y, p_intersection_with_ball, pw_x, p_verti);   

    endgenerate

    logic [2:0] intersection_on;

//        parameter i = 0;
//     box my_box(!nRST, PixelClk, b_c_en[i], b_wr_en[i], i_bx[i], i_by[i], b_colin[i], b_x[i], b_y[i], colour_boxes[i], b_w[i], b_h[i]);
//     defparam my_box.X_LOC = 300;             
//     defparam my_box.WIDTH = BOX_WIDTH;
//     defparam my_box.HEIGHT = 200;
//     defparam my_box.Y_LOC = 0;
//     intersects inter(b_x[i], b_y[i], b_w[i], b_h[i], PixelCount, LineCount, intersection[i], bw_x[i]);
//     intersect_ad inter2(en[i],PixelClk, !nRST, b_x[i], b_y[i], b_w[i], b_h[i], red_x, red_y, intersection_with_ball[i], bw_x[i], verti); 

    always@* INTERSECTED <= | verti;

    always @(posedge PixelClk) begin
        if (intersection_on == 3'd0) begin
            LCD_R  <=  0;
            LCD_G  <=  0;
            LCD_B  <=  0;
        end
        else if (intersection_on == 3'd1) begin
            LCD_R       <=   5'b11111;    
            LCD_G       <=   6'b0;
            LCD_B       <=   5'b0;  
        end
        else if (intersection_on == 3'd2) begin
            LCD_B       <=   5'b0;    
            LCD_R       <=   5'b11111;
            LCD_G       <=   6'b111111;
        end  
        else if (intersection_on == 3'd3) begin
            LCD_B       <=   5'b11111;    
            LCD_R       <=   5'b0;
            LCD_G       <=   6'b0;
        end   
        else if (intersection_on == 3'd4) begin
            LCD_B       <=   5'b0;    
            LCD_R       <=   5'b0;
            LCD_G       <=   6'b111111;
        end 
    end

    logic intersects_ball;
    always@* 
         intersects_ball <= PixelCount >= red_x  && ( PixelCount <= red_x + red_w) && ( LineCount >= red_y) && ( LineCount <= red_y + red_h) ;

    integer x;
    always @(  posedge PixelClk or negedge nRST  )begin                                        
        if( !nRST ) begin
            intersection_on <= 0;
        end
        else if (!DEAD) begin
            if (intersects_ball) begin
                intersection_on <= 1;
            end       
            else if (| intersection) begin            
                intersection_on <= 2;
            end
            else if (p_intersection) begin
                intersection_on <= 4;
            end
            else intersection_on <= 3;
        end
    end

    logic [27:0] counter;    
    logic [27:0] counter_slow;    
    
    parameter COUNT =  28'd550000;
    parameter COUNT2 = 28'd3300000;    
    
    logic dir;
    logic vdir; 
    logic [7:0] killed;

    always@(posedge PixelClk) begin

        if (!nRST)
            counter <= 0;

        else if (counter < COUNT)
            counter <= counter + 1;

        else if (counter == COUNT)
            counter <= 0;

    end

    always@(posedge PixelClk) begin

        if (!nRST)
            counter_slow <= 0;

        else if (counter_slow < COUNT2)
            counter_slow <= counter_slow + 1;

        else if (counter_slow == COUNT2)
            counter_slow <= 0;

    end


    always @ (posedge PixelClk) begin

        if (!nRST) begin
            
            SPEED <= 1;
            DEAD <= 0;
            p_wr_en <= 1;
            wr_en <= 1;
            i_x <= ZERO*3.25;
            i_y <= 250;
            dir <= 1;
            vdir <= 0;
            colin <= 16'b0;
            c_en <= 0;     
            en <= {{(NUM_BOXES){1'b1}}};            
            i_px <= ZERO + (WIDTH-ZERO)/2 - BOX_WIDTH;
            i_py <= 253;
            killed <= 0;

        end

        else if (counter_slow == COUNT2) begin

            if (BTN_B) begin

                p_wr_en <= 1;
                i_px <= p_x + 20;
                i_py <= p_y;

            end

            else if (BTN_A) begin

                p_wr_en <= 1;
                i_px <= p_x - 20;
                i_py <= p_y;

            end

        end

        else if (counter == COUNT) begin                                  

            if (red_x >= (WIDTH - red_w) && dir) begin
                dir <= 0;
                wr_en <= 0;
                p_wr_en <= 0;
                c_en<=1;
                colin <= {5'b0, 6'b111111, 5'b0};
            end

            else if (red_x <= HEIGHT && !dir) begin
                dir <= 1;
                wr_en <= 0;
                c_en<=1;
                p_wr_en <= 0;
                colin <= {colour_box[0], colour_box[15:1]};
            end
            
            else if (red_y <= 32'd1 && !vdir) begin
                vdir <= 1;
                wr_en <= 1;
                i_y <= i_y + 3;
                i_x <= i_x;
                p_wr_en <= 0;
                c_en<=1;
                colin <= {colour_box[0], colour_box[15:1]};
            end

            else if (red_y >= 32'd270 - red_h && vdir) begin
                vdir <= 0;
                wr_en <= 0;
                p_wr_en <= 0;
                c_en<=1;
                colin <= {colour_box[0], colour_box[15:1]};
                DEAD <= 1;
            end

            else if (p_intersection_with_ball) begin
                vdir <= !vdir;
                wr_en <= 1;
                p_wr_en <= 0;
                i_y <= red_y - 6;
                i_x <= red_x;
            end

            else if ( | intersection_with_ball ) begin
    
                for (integer i = 0 ; i < NUM_BOXES; i++) begin

                    if (intersection_with_ball[i]) begin
    
                        en[i] <= 0;
                        killed <= killed + 1;
                        if (killed == NUM_BOXES/2) SPEED<= SPEED + 1;
                        if (verti[i]) begin                             
                            dir <= !dir;
                            if (dir) i_x <= red_x - 5;
                            else i_x <= red_x + 5;
                            wr_en <= 1;
                        end

                       else begin  
                            vdir <= !vdir;
                            if (vdir) i_y <= red_y - 6;
                            else i_y <= red_y + 6;
                            wr_en <= 1;
                        end                                               
                                                                   
                    end

                end
  
            end
               
            else begin
                                
                c_en<=0;
                wr_en <= 1;
                p_wr_en <= 0;
                if (dir) begin
                    i_x <= red_x + SPEED;                    
                end
                else if (!dir) begin 
                    i_x <= red_x - SPEED;                   
                end                

                if (vdir) i_y <= i_y + SPEED;
                else i_y <= i_y - SPEED;

            end

        end

        else begin
            wr_en <= 0;
            p_wr_en <= 0;
        end

    end  
    

endmodule

