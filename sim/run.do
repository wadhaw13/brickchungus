vlib work

vlog "./../src/box.sv"
vlog "./test.sv"

vsim tb

add wave -radix unsigned tb/red_box/* 
add wave -radix unsigned tb/linecount
add wave -radix unsigned tb/hit

force -deposit tb/clk 0 1, 1 10 -r 20
force tb/nRST 1
force tb/red_box/wr_en 0
run 20
force tb/nRST 0
run 20ps


#D://Desktop//Angad//codes//Tang//Tang-Nano-examples-master//example_lcd//lcd_pjt//sim//run.do