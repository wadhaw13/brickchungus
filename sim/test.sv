module tb ();

    logic [9:0] red_x;
    logic [9:0] red_y;
    logic [7:0] red_w;
    logic [7:0] red_h;
    logic [15:0] colour_box;
    logic nRST;
    logic clk;

    box red_box(nRST, clk, wr_en, 10'b0, 10'b0, red_x, red_y, colour_box, red_w, red_h);

    logic [9:0] linecount;
    logic hit;    

    always @(posedge clk) begin

        if (nRST) begin
            hit <= 0;
            linecount <= 0;
        end

        else if (linecount >= red_x && linecount <= red_x + red_w) begin

            hit <= 1;
            linecount <= linecount + 1;

        end

        else if (linecount > 200) 
            linecount <= 0;

        else begin

            hit <= 0;
            linecount <= linecount + 1;

        end

    end


endmodule